{
    let $ = global.$ || {};
    let jQuery = global.jQuery || {};

    $ = jQuery = require('jquery');

    global.$ = $;
    global.jQuery = jQuery;

    require('flexslider');

	{
		$('.flexslider').flexslider();

        $('.flexslider-slide').each(function () {
            $(this).flexslider({
                animation: "slide",
                customDirectionNav: $(this).attr('data-custom-direction-nav') ? $($(this).attr('data-custom-direction-nav') + ' a') : false
            });
        });

        //header mega menu slide
        $('.megamenu-slider').flexslider({
            animation: "slide",
            customDirectionNav: $(this).next('.megamenu-slider-nav a'),
            start: function () {
                var index = $('.header-nav-slide .flex-active-slide').index();
                $(this).parents().find('.c-navigation-featured-content__footer-wrapper').hide();
                $(this).parents().find('.c-navigation-featured-content__footer-wrapper:eq('+ --index +')').show();
            },
            after: function () {
                var index = $('.header-nav-slide .flex-active-slide').index();
                $(this).parents().find('.c-navigation-featured-content__footer-wrapper').hide();
                $(this).parents().find('.c-navigation-featured-content__footer-wrapper:eq('+ --index +')').show();
            }
        });

        //general carousel slide
        $('.c-carousel-slide').flexslider({
            animation: "slide",
            customDirectionNav: $(this).next('.c-carousel-slide-nav a'),
            start: function () {
                var index = $('c-carousel-slide .flex-active-slide').index();
                $(this).parents().find('.c-carousel-complete-the-look__footer').hide();
                $(this).parents().find('.c-carousel-complete-the-look__footer:eq('+ --index +')').show();
            },
            after: function () {
                var index = $('c-carousel-slide .flex-active-slide').index();
                $(this).parents().find('.c-carousel-complete-the-look__footer').hide();
                $(this).parents().find('.c-carousel-complete-the-look__footer:eq('+ --index +')').show();
            }
        });

        //header navigation events
        if ($(window).width() > 700) {
            $('.c-navigation--primary__title-button').on('mouseover', function () {
                var $this = $(this);

                $('.c-navigation-mega-menu').not($this).removeClass('c-navigation-mega-menu--is-visible');

                $this.addClass('active');
                $('.header-overlay').addClass('visible');
                $this.next('.c-navigation-mega-menu').addClass('c-navigation-mega-menu--is-visible').stop().animate({'max-height': $('.c-navigation-mega-menu__inner').height() }, 200);
            });

            $(document).on('mouseleave', '.c-navigation-main-item', function () {
                $('.c-navigation--primary__title-button').removeClass('active');
                $('.c-overlay').removeClass('visible');
                $('.c-navigation-mega-menu').removeClass('c-navigation-mega-menu--is-visible').stop().animate({'max-height': 0 }, 1);
            });

            $(window).on('load scroll', function () {
                if ($(window).scrollTop() > 20) {
                    $('header').addClass('c-header--is-skinny');
                } else {
                    $('header').removeClass('c-header--is-skinny');
                }
            });
        } else {
            $('.mobile-menu-button, .c-navigation__control-close').on('click', function () {
                $('.c-header__navigation').toggleClass('c-header__navigation--is-visible');
                $('.header-overlay').toggleClass('visible');
            });

            var subMenuIndex = 0;
            $('.c-navigation--primary__title-button').on('click', function(e) {
                e.preventDefault();
                var $this = $(this),
                    $subMenu = $this.next('.c-navigation-mega-menu');

                if ($subMenu) {
                    $subMenu.addClass('c-navigation-mega-menu--is-visible');
                    $('.c-navigation__scroller').removeClass('is-level-0 is-level-1 is-level-2 is-level-3').addClass('is-level-' + ++subMenuIndex);
                } else {
                    window.location.href= $this.attr('href');
                }

                if (subMenuIndex > 0) {
                    $('.c-navigation__control-back').addClass('c-navigation__control-back--is-visible')
                }
            });

            $('.c-navigation__control-back').on('click', function (e) {
                e.preventDefault();
                $('.c-navigation__scroller').removeClass('is-level-0 is-level-1 is-level-2 is-level-3').addClass('is-level-' + --subMenuIndex);

                if (subMenuIndex > 0) {
                    $('.c-navigation__control-back').addClass('c-navigation__control-back--is-visible');
                } else {
                    $('.c-navigation__control-back').removeClass('c-navigation__control-back--is-visible');
                }
            });
        }


		//Arama formu gösterme
		$('.show-search, .c-search-input .o-close-button').on('click', function (e) {
            $('.c-search-input').toggleClass('c-search-input--is-active')
        });

        //anasayfa slider
        var $homePageSlider = $('.homepage-campaign-slider');
        $homePageSlider.flexslider({
            animation: "slide",
            customDirectionNav: $('.homepage-campaign-slider-nav a'),
            start: function () {
                var index = $('.homepage-campaign-slider .flex-active-slide').index();
                $homePageSlider.parents('.homepage-editorial-block').find('.homepage-editorial-block-header').removeClass('active');
                $homePageSlider.parents('.homepage-editorial-block').find('.homepage-editorial-block-header:eq('+ --index +')').addClass('active');
            },
            after: function () {
                var index = $('.homepage-campaign-slider .flex-active-slide').index();
                $homePageSlider.parents('.homepage-editorial-block').find('.homepage-editorial-block-header').removeClass('active');
                $homePageSlider.parents('.homepage-editorial-block').find('.homepage-editorial-block-header:eq('+ --index +')').addClass('active');
            }
        });
	}
};